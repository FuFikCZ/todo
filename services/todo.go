package services

import (
	"todo/models"
	"todo/pkg/auth"

	"gorm.io/gorm"
)

const createItemBatchSize = 20

type TODOListDoneLast struct{}

func (TODOListDoneLast) Apply(db *gorm.DB) *gorm.DB {
	return db.Preload("Items", func(db *gorm.DB) *gorm.DB {
		return db.Order("items.done_at IS NULL DESC, items.done_at DESC, items.created_at DESC")
	})
}

type TODOService struct {
	db *gorm.DB
}

func NewTODOService(db *gorm.DB) *TODOService {
	return &TODOService{db: db}
}

func (s *TODOService) ListLists(scopes ...Scope) ([]models.List, error) {
	lists := []models.List{}
	result := s.db.Scopes(applyScopes(scopes)).Find(&lists)
	return lists, result.Error
}

func (s *TODOService) CreateList(owner auth.User, name string) (models.List, error) {
	newList := models.List{Name: name, Owner: owner}
	result := s.db.Create(&newList)
	return newList, result.Error
}

func (s *TODOService) GetListByID(id uint, scopes ...Scope) (models.List, error) {
	list := models.List{Items: []models.Item{}}
	result := s.db.Scopes(applyScopes(scopes)).First(&list, id)
	return list, result.Error
}

func (s *TODOService) AddItemToList(owner auth.User, list models.List, name string) (models.Item, error) {
	newItem := models.Item{Name: name, ListID: list.ID, Owner: owner}
	result := s.db.Create(&newItem)
	return newItem, result.Error
}

func (s *TODOService) AddItemsToList(items []models.Item) error {
	result := s.db.CreateInBatches(&items, createItemBatchSize)
	return result.Error
}

// _, err := DB.Exec("UPDATE items SET completed_at = CASE WHEN completed_at IS NULL THEN CURRENT_TIMESTAMP ELSE NULL END WHERE id = ?", id)

func (s *TODOService) ToggleItemDone(owner auth.User, item models.Item) error {
	item.DoneBy = owner
	result := s.db.Model(&item).Updates(map[string]interface{}{
		"done_at":    gorm.Expr("CASE WHEN done_at IS NULL THEN CURRENT_TIMESTAMP ELSE NULL END"),
		"done_by_id": gorm.Expr("CASE WHEN done_by_id IS NULL THEN ? ELSE NULL END", owner.ID),
	})

	// "completed_at", clause.Expr(gorm.Expr("CASE WHEN completed_at IS NULL THEN CURRENT_TIMESTAMP ELSE NULL END")))
	return result.Error
}

func (s *TODOService) DeleteItem(owner auth.User, item models.Item) error {
	result := s.db.Where("list_id", item.ListID).Delete(&item)
	return result.Error
}

func (s *TODOService) GetUserStatistics(u auth.User) (models.Statistics, error) {
	stats := models.Statistics{
		TotalLists:           0,
		TotalItemsCreated:    0,
		TotalItemsDone:       0,
		Top5Items:            []models.Item{},
		Top5ItemsCount:       map[string]int64{},
		LongestItemName:      "",
		LongestList:          "",
		LongestListItemCount: 0,
	}

	if result := s.db.Model(&models.List{}).Where("owner_id = ?", u.ID).Count(&stats.TotalLists); result.Error != nil {
		return stats, result.Error
	}

	if result := s.db.Model(&models.Item{}).Where("owner_id = ?", u.ID).Count(&stats.TotalItemsCreated); result.Error != nil {
		return stats, result.Error
	}

	if result := s.db.Model(&models.Item{}).Where("owner_id = ? AND done_at IS NOT NULL", u.ID).Count(&stats.TotalItemsDone); result.Error != nil {
		return stats, result.Error
	}

	// if result := s.db.Model(&models.Item{}).Where("owner_id = ?", u.ID).Order("created_at DESC").Limit(5).Find(&stats.Top5Items); result.Error != nil {
	// 	return stats, result.Error
	// }

	rows, err := s.db.Model(&models.Item{}).Where("owner_id = ?", u.ID).Select("name, COUNT(*) as count").Group("name").Order("count DESC").Limit(5).Rows()
	if err != nil {
		return stats, err
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		var count int64
		if err := rows.Scan(&name, &count); err != nil {
			return stats, err
		}
		stats.Top5ItemsCount[name] = count
		stats.Top5Items = append(stats.Top5Items, models.Item{Name: name})
	}

	if result := s.db.Model(&models.Item{}).Where("owner_id = ?", u.ID).Select("name").Group("name").Order("LENGTH(name) DESC").Limit(1).Scan(&stats.LongestItemName); result.Error != nil {
		return stats, result.Error
	}

	// if result := s.db.Model(&models.List{}).Where("owner_id = ?", u.ID).Select("name, COUNT(*) as count").Group("name").Order("count DESC").Limit(1).Scan(&stats.LongestList); result.Error != nil {
	// 	return stats, result.Error
	// }

	// if result := s.db.Model(&models.List{}).Where("owner_id = ?", u.ID).Select("name, COUNT(*) as count").Group("name").Order("count DESC").Limit(1).Scan(&stats.LongestListItemCount); result.Error != nil {
	// 	return stats, result.Error
	// }

	return stats, nil
}
