package services

import (
	"gorm.io/gorm"
)

type Service struct {
	db *gorm.DB
}

type Scope interface {
	Apply(db *gorm.DB) *gorm.DB
}

func (s *Service) Init(db *gorm.DB) {
	s.db = db
}

type PreloadOpts []string

func (o PreloadOpts) Apply(db *gorm.DB) *gorm.DB {
	for _, preload := range o {
		db = db.Preload(preload)
	}
	return db
}

type JoinOpts []string

func (j JoinOpts) Apply(db *gorm.DB) *gorm.DB {
	for _, join := range j {
		db = db.Joins(join)
	}
	return db
}

type FilterOpts struct {
	Query interface{}
	Args  []interface{}
}

func (o FilterOpts) Apply(db *gorm.DB) *gorm.DB {
	return db.Where(o.Query, o.Args...)
}

type ListOpts struct {
	Order  string
	Limit  int
	Offset int
}

func (o ListOpts) Apply(db *gorm.DB) *gorm.DB {
	if o.Order != "" {
		db = db.Order(o.Order)
	}

	if o.Limit == 0 {
		o.Limit = 10
	}

	return db.Limit(o.Limit).Offset(o.Offset)
}

func applyScopes(scopes []Scope) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		for _, s := range scopes {
			db = s.Apply(db)
		}
		return db
	}
}
