package models

import (
	"database/sql"
	"todo/pkg/auth"

	"github.com/samber/lo"
	"gorm.io/gorm"
)

type List struct {
	gorm.Model
	Name    string
	Owner   auth.User
	OwnerID uint
	Items   []Item
}

func (l List) ItemsTotal() int {
	return len(l.Items)
}
func (l List) ItemsDone() int {
	return lo.CountBy(l.Items, func(i Item) bool { return i.DoneAt.Valid })
}

type Item struct {
	gorm.Model
	Name     string
	ListID   uint
	Owner    auth.User
	OwnerID  uint
	DoneAt   sql.NullTime
	DoneBy   auth.User
	DoneByID sql.NullInt32
}

type Statistics struct {
	TotalLists        int64
	TotalItemsCreated int64
	TotalItemsDone    int64
	Top5Items         []Item
	Top5ItemsCount    map[string]int64

	LongestItemName      string
	LongestList          string
	LongestListItemCount int64
}
