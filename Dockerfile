FROM node:latest AS buildcss
WORKDIR /app
COPY . .
RUN ls -la
RUN npm install -D tailwindcss
RUN npx tailwindcss build -c tailwind.config.js -i css/input.css -o output.css

FROM golang:alpine AS buildgo
WORKDIR /app
COPY go.mod go.sum ./
COPY --from=buildcss /app/output.css /app/static/css/output.css
RUN go mod download && go install github.com/a-h/templ/cmd/templ@latest

COPY . .
RUN TEMPL_EXPERIMENT=rawgo templ generate && go build -o ./app .


FROM scratch
WORKDIR /app
COPY --from=buildgo /app/app .
# Start the server
CMD ["./app"]
