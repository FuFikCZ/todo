package view

import (
	"context"
	"todo/pkg/auth"

	"github.com/a-h/templ"
	"github.com/labstack/echo/v4"
)

func Render(c echo.Context, component templ.Component) error {
	return component.Render(context.WithValue(c.Request().Context(), auth.UserContextKey, c.Get(string(auth.UserContextKey))), c.Response())
}
