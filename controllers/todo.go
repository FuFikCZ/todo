package controllers

import (
	"log/slog"
	"net/http"
	"strconv"
	"strings"
	"todo/models"
	"todo/pkg/auth"
	"todo/services"
	"todo/view"
	"todo/view/pages"
	"todo/view/todo"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type TODOController struct {
	TODOService *services.TODOService
}

func (h *TODOController) RegisterHandlers(router *echo.Group) {
	router.GET("", h.handleIndex)
	router.GET("/lists", h.handleListLists)
	router.POST("/new", h.handleNewList, auth.RequireLoginMiddleware)
	router.GET("/list/:listID", h.handleViewList)
	router.GET("/list/:listID/import", h.handleModalImport)
	router.POST("/list/:listID/import", h.handleImportList, auth.RequireLoginMiddleware)
	router.POST("/list/:listID", h.handleAddItemToList, auth.RequireLoginMiddleware)
	router.Any("/list/:listID/toggle/:itemID", h.handleToggleItemDone, auth.RequireLoginMiddleware)
	router.Any("/list/:listID/delete/:itemID", h.handleDeleteItem, auth.RequireLoginMiddleware)
	router.GET("/profile", h.handleProfile, auth.RequireLoginMiddleware)
}

func (h *TODOController) handleListLists(c echo.Context) error {
	// TODO: use bind and validation
	query := c.QueryParam("query")
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		limit = 5
	}
	lists, err := h.TODOService.ListLists(
		services.JoinOpts{"Owner"},
		services.PreloadOpts{"Items"},
		services.ListOpts{Limit: limit, Order: "lists.created_at desc"},
		services.FilterOpts{Query: "lists.name like ?", Args: []interface{}{"%" + query + "%"}},
	)
	if err != nil {
		return err
	}
	return view.Render(c, todo.TODOListList(todo.TODOListListProps{TODOLists: lists}))
}

func (h *TODOController) handleIndex(c echo.Context) error {
	lists, err := h.TODOService.ListLists(
		services.JoinOpts{"Owner"},
		services.PreloadOpts{"Items"},
		services.ListOpts{Limit: 5, Order: "lists.created_at desc"},
	)
	if err != nil {
		return err
	}
	return view.Render(c, pages.Index(lists))
}

func (h *TODOController) handleNewList(c echo.Context) error {
	// TODO: use bind and validation
	name := c.FormValue("name")

	list, err := h.TODOService.CreateList(auth.UserFromEchoContext(c), name)
	if err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, "/list/"+strconv.Itoa(int(list.ID)))
}

func (h *TODOController) handleViewList(c echo.Context) error {
	// TODO: use bind and validation
	listID := c.Param("listID")
	id, err := strconv.Atoi(listID)
	if err != nil {
		return err
	}

	list, err := h.TODOService.GetListByID(
		uint(id),
		services.JoinOpts{"Owner"},
		services.TODOListDoneLast{},
	)
	if err != nil {
		return err
	}

	if c.Request().Header.Get("HX-Request") == "true" {
		return view.Render(c, todo.TODOList(todo.TODOListProps{Items: list.Items}))
	}

	return view.Render(c, pages.TodoList(list))
}

func (h *TODOController) handleAddItemToList(c echo.Context) error {
	// TODO: use bind and validation
	listID := c.Param("listID")
	id, err := strconv.Atoi(listID)
	if err != nil {
		return err
	}

	// TODO: use bind and validation
	itemName := c.FormValue("name")

	_, err = h.TODOService.AddItemToList(
		auth.UserFromEchoContext(c),
		models.List{Model: gorm.Model{ID: uint(id)}},
		itemName,
	)

	if err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, "/list/"+listID)
}

func (h *TODOController) handleToggleItemDone(c echo.Context) error {
	// TODO: use bind and validation
	listID := c.Param("listID")
	itemID := c.Param("itemID")
	itemIDInt, err := strconv.Atoi(itemID)
	if err != nil {
		return err
	}

	item := models.Item{Model: gorm.Model{ID: uint(itemIDInt)}}

	err = h.TODOService.ToggleItemDone(
		auth.UserFromEchoContext(c),
		item,
	)

	if err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, "/list/"+listID)
}

type deleteItemRequest struct {
	ListID uint `param:"listID"`
	ItemID uint `param:"itemID"`
}

func (h *TODOController) handleDeleteItem(c echo.Context) error {
	itemSelector := deleteItemRequest{}
	err := c.Bind(&itemSelector)
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	item := models.Item{Model: gorm.Model{ID: itemSelector.ItemID}, ListID: itemSelector.ListID}

	slog.Info("Deleting item", "item", itemSelector)

	err = h.TODOService.DeleteItem(
		auth.UserFromEchoContext(c),
		item,
	)

	if err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, "/list/"+strconv.Itoa(int(itemSelector.ListID)))
}

type modalImportProps struct {
	ListID uint `param:"listID"`
}

func (h *TODOController) handleModalImport(c echo.Context) error {
	props := modalImportProps{}
	err := c.Bind(&props)
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}
	return view.Render(c, todo.TODOImportModal(todo.TODOImportModalProps{ListID: props.ListID}))
}

type formImportProps struct {
	ListID uint   `param:"listID"`
	List   string `form:"list"`
}

func (h *TODOController) handleImportList(c echo.Context) error {
	props := formImportProps{}
	err := c.Bind(&props)
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	// do not import items when list does not exist
	_, err = h.TODOService.GetListByID(props.ListID)
	if err != nil {
		return err
	}

	newItems := []models.Item{}
	for _, line := range strings.Split(props.List, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		newItems = append(newItems, models.Item{Name: line, ListID: props.ListID, Owner: auth.UserFromEchoContext(c)})
	}

	if err = h.TODOService.AddItemsToList(newItems); err != nil {
		return err
	}

	return c.Redirect(http.StatusSeeOther, "/list/"+strconv.Itoa(int(props.ListID)))
}

func (h *TODOController) handleProfile(c echo.Context) error {
	stats, err := h.TODOService.GetUserStatistics(auth.UserFromEchoContext(c))
	if err != nil {
		return err
	}

	return view.Render(c, pages.Profile(auth.UserFromEchoContext(c), stats))
}
