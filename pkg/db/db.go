package db

import (
	"log/slog"
	"os"

	slogGorm "github.com/orandin/slog-gorm"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	gormLogger "gorm.io/gorm/logger"
)

func NewDB(logger *slog.Logger) *gorm.DB {
	db, err := gorm.Open(postgres.Open(os.Getenv("POSTGRES_DSN")), &gorm.Config{
		Logger: slogGorm.New(
			slogGorm.WithHandler(logger.Handler()),
			slogGorm.SetLogLevel(slogGorm.DefaultLogType, slog.LevelInfo),
			slogGorm.WithSlowThreshold(0),
		).LogMode(gormLogger.Info),
	})
	if err != nil {
		panic("failed to connect database")
	}

	return db
}
