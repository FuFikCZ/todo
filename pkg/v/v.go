package v

import (
	"strings"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

// func
var (
	// validate *validator.Validate
	// trans    ut.Translator
	enL      = en.New()
	uni      = ut.New(enL, enL)
	trans, _ = uni.GetTranslator("en")
	validate = validator.New()
)

type FormErrors map[string]string

func (f FormErrors) HasError(field string) bool {
	_, ok := f[field]
	return ok
}

func (f FormErrors) GetError(field string) string {
	return f[field]
}

type Validator struct{}

func (v *Validator) Validate(i interface{}) error {
	return validate.Struct(i)
}

func NewFormErrors(err error) FormErrors {
	errs, ok := err.(validator.ValidationErrors)
	if !ok {
		return nil
	}

	errors := FormErrors{}
	for _, e := range errs {
		errors[strings.ToLower(e.Field())] = e.Translate(trans)

	}

	return errors
}

// func RenderFormErrors[T any](c echo.Context, form T, err error, errorpage func(T, FormErrors) templ.Component) error {
// 	errs, ok := err.(validator.ValidationErrors)
// 	if !ok {
// 		return err
// 	}

// 	return c.Render(http.StatusBadRequest, errorpage(form, FormErrors(errs.Translate(trans))))
// }

func init() {
	// initialize validator with english translations
	// en := en.New()
	// uni := ut.New(en, en)
	// // this is usually know or extracted from http 'Accept-Language' header
	// // also see uni.FindTranslator(...)
	// trans, _ := uni.GetTranslator("en")
	// validate = validator.New()
	en_translations.RegisterDefaultTranslations(validate, trans)
}
