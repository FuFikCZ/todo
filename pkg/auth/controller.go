package auth

import (
	"errors"
	"log/slog"
	"net/http"
	"time"
	"todo/pkg/v"

	"github.com/a-h/templ"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type AuthController struct {
	AuthService          *AuthService
	LoginPage            func(FormLoginCredentials, v.FormErrors) templ.Component
	RegisterPage         func(FormRegisterUserInfo, v.FormErrors) templ.Component
	RuccessLoginRedirect string
	Render               func(echo.Context, templ.Component) error
}

func (h *AuthController) RegisterHandlers(router *echo.Group) {
	router.GET("/register", h.handleRegisterIndex)
	router.POST("/register", h.handleRegisterPost)
	router.GET("/login", h.handleLoginIndex)
	router.POST("/login", h.handleLoginPost)
	router.GET("/logout", h.handleLogout)
}

func (h *AuthController) handleRegisterIndex(c echo.Context) error {
	return h.Render(c, h.RegisterPage(FormRegisterUserInfo{}, nil))
}

func (h *AuthController) handleRegisterPost(c echo.Context) error {
	userInfo := FormRegisterUserInfo{}
	err := c.Bind(&userInfo)
	if err != nil {
		return err
	}

	if err := c.Validate(userInfo); err != nil {
		for k, e := range v.NewFormErrors(err) {
			slog.Warn("Validation error", "k", k, "e", e)
		}
		return h.Render(c, h.RegisterPage(userInfo, v.NewFormErrors(err)))
	}

	user, err := h.AuthService.NewUser(c.Request().Context(), userInfo.Email, userInfo.Name)
	if err != nil {
		return err
	}
	err = h.AuthService.NewLocalAccount(c.Request().Context(), user, userInfo.Email, userInfo.Password)
	if err != nil {
		return err
	}
	return c.Redirect(http.StatusSeeOther, "/login")
}

func (h *AuthController) handleLoginIndex(c echo.Context) error {
	return h.Render(c, h.LoginPage(FormLoginCredentials{}, nil))
}

func (h *AuthController) handleLoginPost(c echo.Context) error {
	redirect := c.QueryParam("next")
	if redirect == "" {
		redirect = h.RuccessLoginRedirect
	}

	credentials := FormLoginCredentials{}
	err := c.Bind(&credentials)
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}
	user, err := h.AuthService.Login(c.Request().Context(), credentials.Email, credentials.Password)
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return h.Render(c, h.LoginPage(FormLoginCredentials{Email: credentials.Email}, v.FormErrors{
				"password": "Incorrect email or password",
			}))
		}
		return err
	}

	token, err := h.AuthService.UserToken(c.Request().Context(), user)
	if err != nil {
		return err
	}

	cookie := http.Cookie{
		Name:    "authToken",
		Value:   string(token),
		Expires: time.Now().Add(24 * 7 * time.Hour),
	}

	c.SetCookie(&cookie)
	return c.Redirect(http.StatusSeeOther, redirect)

}

func (h *AuthController) handleLogout(c echo.Context) error {
	cookie := http.Cookie{
		Name:    "authToken",
		Expires: time.Unix(0, 0),
	}
	c.SetCookie(&cookie)
	return h.Render(c, h.LoginPage(FormLoginCredentials{}, nil))
}
