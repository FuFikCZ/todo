package auth

import (
	"encoding/json"
	"log/slog"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

func RequireLoginMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if user, ok := c.Get(string(UserContextKey)).(User); !ok || user.ID == 0 {
			return c.Redirect(http.StatusSeeOther, "/login?next="+c.Request().RequestURI)
		}
		return next(c)
	}
}

func (s *AuthService) AuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		token, err := c.Cookie("authToken")
		if err != nil {
			unsetUser(c)
			return next(c)
		}
		t, err := jwt.ParseString(token.Value, jwt.WithKeySet(s.PublicKey))
		if err != nil {
			slog.Warn("invalid token", slog.String("error", err.Error()))
			unsetUser(c)
			return next(c)
		}
		payload, ok := t.Get("userInfo")
		if !ok {
			slog.Warn("no userinfo in token")
			unsetUser(c)
			return next(c)
		}

		marshaled, err := json.Marshal(payload)
		if err != nil {
			slog.Warn("failed to marshal userinfo", slog.String("error", err.Error()))
			unsetUser(c)
			return next(c)
		}

		u := User{}
		err = json.Unmarshal(marshaled, &u)
		if err != nil {
			slog.Warn("failed to unmarshal userinfo", slog.String("error", err.Error()))
			unsetUser(c)
			return next(c)
		}

		c.Set(string(UserContextKey), u)

		return next(c)
	}
}
