package auth

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type ContextKey string

const UserContextKey ContextKey = "user"
const rsaKeySize = 2048
const jwtExpiration = time.Hour * 24 * 7
const jwtIssuer = "https://www.rezervate-listku.cz"

var jwtAudience = []string{"https://www.rezervate-listku.cz"}

var AnonymousUser = User{Model: gorm.Model{ID: 1}, Email: "anonymous", Name: "Anonymous"}

type AuthService struct {
	db         *gorm.DB
	PrivateKey jwk.Set
	PublicKey  jwk.Set
}

func NewAuthService(db *gorm.DB, privateKeyFileName, publicKeyFileName string) *AuthService {
	s := &AuthService{}
	var err error
	s.db = db

	s.PrivateKey, s.PublicKey, err = loadKeyPair(privateKeyFileName, publicKeyFileName)
	if err != nil {
		slog.Warn("no or invalid JWKS, generating new", slog.String("error", err.Error()))
		s.PrivateKey, s.PublicKey, err = newKeyPair(privateKeyFileName, publicKeyFileName)
		if err != nil {
			slog.Error("failed to create new JWKS", slog.String("error", err.Error()))
		}
	}

	return s
}

func unsetUser(c echo.Context) {
	cookie := http.Cookie{
		Name:    "authToken",
		Expires: time.Unix(0, 0),
	}
	c.SetCookie(&cookie)
	c.Set(string(UserContextKey), AnonymousUser)
}

func (s *AuthService) Login(c context.Context, email, password string) (u User, err error) {
	auth := LocalAccount{Email: email}
	result := s.db.Where(&auth).Preload("User").First(&auth)
	if err = result.Error; err != nil {
		return
	}

	if err = bcrypt.CompareHashAndPassword([]byte(auth.Password), []byte(password)); err != nil {
		return
	}
	u = auth.User
	return
}

func (s *AuthService) UserToken(c context.Context, u User) (token []byte, err error) {
	signingKey, ok := s.PrivateKey.Key(0)
	if !ok {
		err = fmt.Errorf("signing key not found")
		return
	}
	return jwtForUser(signingKey, u)
}

func (s *AuthService) NewLocalAccount(c context.Context, user User, email, password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	result := s.db.Create(&LocalAccount{
		Email:    email,
		Password: string(hashedPassword),
		User:     user,
	})

	return result.Error
}

func (s *AuthService) ListUsers(c context.Context) ([]User, error) {
	userList := []User{}
	result := s.db.Model(&User{}).Find(&userList)
	return userList, result.Error
}

func (s *AuthService) NewUser(c context.Context, email, name string) (User, error) {
	newUser := User{Email: email, Name: name}
	result := s.db.Create(&newUser)
	if result.Error != nil {
		return User{}, result.Error
	}
	result = s.db.First(&newUser, newUser.ID)
	if result.Error != nil {
		return User{}, result.Error
	}

	return newUser, nil
}

func UserFromContext(ctx context.Context) User {
	return ctx.Value(UserContextKey).(User)
}

func UserFromEchoContext(ctx echo.Context) User {
	return ctx.Get(string(UserContextKey)).(User)
}
