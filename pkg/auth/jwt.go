package auth

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"os"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

func jwtForUser(signingKey jwk.Key, u User) (token []byte, err error) {
	now := time.Now()
	expiration := now.Add(jwtExpiration)

	unsignedToken, err := jwt.NewBuilder().
		Audience(jwtAudience).
		Expiration(expiration).
		IssuedAt(now).
		Issuer(jwtIssuer).
		NotBefore(now).
		Subject(u.Email).
		Claim("userInfo", u).
		Build()
	if err != nil {
		return
	}

	return jwt.NewSerializer().Sign(jwt.WithKey(jwa.RS512, signingKey)).Serialize(unsignedToken)
}

func loadKeyPair(privateKeyFileName, publicKeyFileName string) (privateKey jwk.Set, publicKey jwk.Set, err error) {
	privateKey, err = jwk.ReadFile(privateKeyFileName)
	if err != nil {
		return
	}

	publicKey, err = jwk.ReadFile(publicKeyFileName)
	if err != nil {
		return
	}

	return
}

func newKeyPair(privateKeyFileName, publicKeyFileName string) (privateKey jwk.Set, publicKey jwk.Set, err error) {
	privateKeyRSA, err := rsa.GenerateKey(rand.Reader, rsaKeySize)
	if err != nil {
		return
	}

	jwkKey, err := jwk.FromRaw(privateKeyRSA)
	if err != nil {
		return
	}

	err = jwkKey.Set(jwk.AlgorithmKey, jwa.RS512)
	if err != nil {
		return
	}
	if err = jwk.AssignKeyID(jwkKey); err != nil {
		return
	}

	privateKey = jwk.NewSet()
	if err = privateKey.AddKey(jwkKey); err != nil {
		return
	}

	publicKey, err = jwk.PublicSetOf(privateKey)
	if err != nil {
		return
	}

	privateKeyFile, err := os.OpenFile(privateKeyFileName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return
	}
	if err = json.NewEncoder(privateKeyFile).Encode(privateKey); err != nil {
		return
	}

	publicKeyFile, err := os.OpenFile(publicKeyFileName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return
	}
	if err = json.NewEncoder(publicKeyFile).Encode(publicKey); err != nil {
		return
	}

	return
}
