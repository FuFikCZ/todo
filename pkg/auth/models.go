package auth

import "gorm.io/gorm"

type LocalAccount struct {
	gorm.Model
	Email    string `gorm:"uniqueIndex"`
	Password string
	UserID   uint
	User     User
}

type User struct {
	gorm.Model
	Name  string
	Email string `gorm:"uniqueIndex"`
}

func (u User) IsAnonymous() bool {
	return u.ID == AnonymousUser.ID
}
