package server

import (
	"log/slog"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Server struct {
	e *echo.Echo
}

func NewServer(config Config) *Server {
	e := echo.New()
	e.IPExtractor = echo.ExtractIPDirect()
	e.StaticFS("/static", config.StaticFS)
	e.Use(RequestLoggerMiddleware)
	e.Pre(middleware.RemoveTrailingSlash())

	if config.InitFN != nil {
		config.InitFN(e)
	}
	return &Server{
		e: e,
	}
}

func (s *Server) RegisterController(pathPrefix string, controller Controller) {
	router := s.e.Group(pathPrefix)
	controller.RegisterHandlers(router)
}

func (s *Server) Start(addr string) {
	s.e.Logger.Info("Routes:")
	for _, r := range s.e.Routes() {
		slog.Info("registered route", slog.String("method", r.Method), slog.String("path", r.Path))
	}
	s.e.Logger.Fatal(s.e.Start(":8080"))
}
