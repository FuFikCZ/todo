package server

import (
	"io/fs"

	"github.com/labstack/echo/v4"
)

type Config struct {
	StaticFS fs.FS
	InitFN   func(e *echo.Echo)
}
