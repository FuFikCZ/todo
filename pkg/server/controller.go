package server

import "github.com/labstack/echo/v4"

type Controller interface {
	RegisterHandlers(router *echo.Group)
}
