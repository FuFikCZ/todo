package server

import (
	"context"
	"log/slog"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var RequestLoggerMiddleware = middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
	LogStatus:      true,
	LogMethod:      true,
	LogURI:         true,
	LogUserAgent:   true,
	LogReferer:     true,
	LogRemoteIP:    true,
	LogRoutePath:   true,
	LogQueryParams: []string{"next"},
	HandleError:    true,
	LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
		if v.Error == nil {
			slog.LogAttrs(context.Background(), slog.LevelInfo, v.Method,
				slog.String("route", v.RoutePath),
				slog.String("uri", v.URI),
				slog.Int("status", v.Status),
				slog.String("clientIP", v.RemoteIP),
				slog.String("clientAgent", v.UserAgent),
			)
		} else {
			slog.LogAttrs(context.Background(), slog.LevelError, "REQUEST_ERROR",
				slog.String("uri", v.URI),
				slog.Int("status", v.Status),
				slog.String("err", v.Error.Error()),
				slog.String("clientIP", v.RemoteIP),
				slog.String("clientAgent", v.UserAgent),
			)
		}

		return nil
	},
})
