package main

import (
	"log/slog"

	"os"
	"todo/controllers"
	"todo/models"
	"todo/pkg/auth"
	"todo/pkg/db"
	"todo/pkg/server"
	"todo/pkg/v"
	"todo/services"
	"todo/static"
	"todo/view"
	"todo/view/pages"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/lmittmann/tint"
	"gorm.io/gorm/clause"
)

func main() {

	logger := slog.New(tint.NewHandler(os.Stdout, nil))
	slog.SetDefault(logger)

	if err := godotenv.Load(); err != nil {
		slog.Warn(err.Error())
	}

	// Init db and migrate
	db := db.NewDB(logger)
	db.AutoMigrate(&auth.User{}, &auth.LocalAccount{}, &models.List{}, &models.Item{})
	db.Clauses(clause.OnConflict{DoNothing: true}).Create(&auth.AnonymousUser)

	// Set up services
	authService := auth.NewAuthService(db, "private.jwks", "public.jwks")
	todoService := services.NewTODOService(db)

	// Set up controllers
	authController := &auth.AuthController{
		AuthService: authService,

		RuccessLoginRedirect: "/",

		Render:       view.Render,
		LoginPage:    pages.Login,
		RegisterPage: pages.Register,
	}

	todoController := &controllers.TODOController{
		TODOService: todoService,
	}

	// Set up server
	server := server.NewServer(server.Config{
		StaticFS: static.FS,
		InitFN: func(e *echo.Echo) {
			e.Validator = &v.Validator{}
			e.Use(authService.AuthMiddleware)
		},
	})

	// register controllers
	server.RegisterController("", authController)
	server.RegisterController("", todoController)

	server.Start(":8080")
}
