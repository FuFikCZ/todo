package ui

import (
	"github.com/a-h/templ"
)

const ErrorTextClasses = "w-full ps-2 text-sm text-red-700 dark:text-red-700"

func applyAttrs(attrs *templ.Attributes, opts ...func(*templ.Attributes)) {
	for _, opt := range opts {
		opt(attrs)
	}
}

func appendClass(attrs *templ.Attributes, class string) {
	attr := *attrs
	if _, ok := attr["class"]; !ok {
		attr["class"] = class
		return
	}
	attr["class"] = attr["class"].(string) + " " + class
}

func prependClass(attrs *templ.Attributes, class string) {
	attr := *attrs
	if _, ok := attr["class"]; !ok {
		attr["class"] = class
		return
	}
	attr["class"] = class + " " + attr["class"].(string)
}

func WithClass(class string) func(*templ.Attributes) {
	return func(attrs *templ.Attributes) {
		appendClass(attrs, class)
	}
}

func ClassIfTrue(class string, condition bool) func(*templ.Attributes) {
	return func(attrs *templ.Attributes) {
		if condition {
			prependClass(attrs, class)
		}
	}
}
