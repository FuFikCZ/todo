package ui

import "github.com/a-h/templ"

const buttonBaseClasses = "inline-flex font-bold items-center justify-center m-1 p-2 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 border"
const buttonPrimaryClass = "dark:bg-blue-500 dark:text-white bg-blue-500 border-blue-500 dark:border-blue-500"
const buttonDangerClass = "dark:bg-red-500 dark:text-white bg-red-500 text-white border-red-900 dark:border-red-900"

type ButtonVariant string

const (
	ButtonPrimary ButtonVariant = "primary"
	ButtonDanger  ButtonVariant = "danger"
)

func ButtonAttrs(buttonType ButtonVariant, opts ...func(*templ.Attributes)) templ.Attributes {
	attrs := templ.Attributes{
		"class": buttonBaseClasses,
	}

	switch buttonType {
	case ButtonPrimary:
		appendClass(&attrs, buttonPrimaryClass)
	case ButtonDanger:
		appendClass(&attrs, buttonDangerClass)
	}

	applyAttrs(&attrs, opts...)

	return attrs
}
