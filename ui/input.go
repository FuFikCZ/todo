package ui

import (
	"log/slog"

	"github.com/a-h/templ"
)

const inputBaseClasses = "w-full m-1 p-2 ps-4 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 text-gray-900 border dark:bg-slate-900 dark:text-slate-400"
const inputPrimaryClasses = "dark:text-white border-slate-400 dark:border-slate-400"
const inputErrorClasses = "border-red-500 dark:border-red-500 focus:border-red-500 focus:ring-red-500"

type InputVariant string

const (
	InputPrimary InputVariant = "primary"
	InputError   InputVariant = "error"
)

func InputAttrs(variant InputVariant, opts ...func(*templ.Attributes)) templ.Attributes {
	attrs := templ.Attributes{
		"class": inputBaseClasses,
	}
	slog.Info(string(variant))

	switch variant {
	case InputPrimary:
		appendClass(&attrs, inputPrimaryClasses)
	case InputError:
		appendClass(&attrs, inputErrorClasses)
	}

	applyAttrs(&attrs, opts...)

	return attrs
}
